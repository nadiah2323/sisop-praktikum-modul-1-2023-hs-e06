#!/bin/bash

syslog="/var/log/syslog"
hour="$(date "+%H")" 
namefile="$(date "+%H:%M %d-%m-%Y").txt"

lower=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
upper=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

lowercase="${lower[$hour]}"
uppercase="${upper[$hour]}"

backup="$(cat $syslog |tr '[a-z]' "[$lowercase-za-$lowercase]" | tr '[A-Z]' "[$uppercase-ZA-$uppercase]")"
echo "$backup" >  /home/akmalx/Documents/"$namefile"
