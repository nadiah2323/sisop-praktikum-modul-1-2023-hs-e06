#!/bin/bash

encrypted_file="/home/akmalx/Documents/20:43 03-03-2023.txt"
hour="$(date -r "$encrypted_file" "+%H")"

lower=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
upper=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

lowercase="${lower[$hour]}"
uppercase="${upper[$hour]}"

decrypted="$(cat "$encrypted_file" | tr "[$lowercase-za-$lowercase]" '[a-z]' | tr "[$uppercase-ZA-$uppercase]" '[A-Z]')"
echo "$decrypted"
