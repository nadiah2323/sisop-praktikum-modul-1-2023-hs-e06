#!/bin/bash

echo -e 'Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.\n'
awk -F',' '{gsub(/"/,"");if($3=="JP" && $4=="Japan") print $1,$2}' OFS='\t'  '2023 QS World University Rankings.csv' |head -n 5| sort -n

echo -e '\ncari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di jepang .\n'
awk -F',' '{gsub(/"/,"");if($3=="JP" && $4=="Japan" ) print $2,$9}' OFS='\t' '2023 QS World University Rankings.csv' |sort -t$'\t' -k2g |head -n 5

echo -e '\ncari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.\n'
awk -F',' '{gsub(/"/,"");if($3=="JP" && $4=="Japan") print $2,$20}' OFS='\t' '2023 QS World University Rankings.csv' |sort -t$'\t' -k2gr |head -n 10 

echo -e '\ncari Universitas dengan kata kunci keren.\n'
grep -E  'keren|Keren' '2023 QS World University Rankings.csv' | awk -F',' '{gsub(/"/,""); print $2}'
